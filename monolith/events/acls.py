import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # api endpoint
    url = "https://api.pexels.com/v1/search"
    # extra parameters for searching a specific city and state (abbreviation)
    # 'query' is a valid param for this specific api
    params = {"query": city + " " + state}
    # this api also requires 'Authorization' to be in the header
    headers = {"Authorization": PEXELS_API_KEY}

    # making the actual response
    response = requests.get(url, params=params, headers=headers)

    # parse/convert the json-format data into python data
    parsed_json = json.loads(response.content)
    # create a dictionary that only contains whatever info we need
    picture = {
        "picture_url": parsed_json["photos"][0]["src"]["original"],
    }
    return picture


def get_lat_lon(location):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }

    response = requests.get(url, params=params)
    parsed_json = json.loads(response.content)
    lat_lon = {
        "latitude": parsed_json["lat"],
        "longitude": parsed_json["lon"],
    }
    return lat_lon


def get_weather(location):
    lat_lon = get_lat_lon(location)

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_lon["latitude"],
        "lon": lat_lon["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    response = requests.get(url, params=params)
    parse_json = json.loads(response.content)
    weather = {
        "temp": parse_json["main"]["temp"],
        "description": parse_json["weather"][0]["description"],
    }
    return weather
