import json
import requests
from .models import ConferenceVO


def get_conferences():
    # This points to the api_list_conference view (in monolith/events)
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    # loop thru the response's content at key "conferences"
    for conference in content["conferences"]:
        # Update existing or create new
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            # a dictionary with key "name" and value of the conference's name
            defaults={"name": conference["name"]},
        )
