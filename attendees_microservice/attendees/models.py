from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist


class AccountVO(models.Model):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    # is_active = models.BooleanField()
    updated = models.DateField()


class ConferenceVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Attendee(models.Model):
    """
    The Attendee model represents someone that wants to attend
    a conference
    """

    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        ConferenceVO,
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})

    def create_badge(self):
        try:
            self.badge  # check the attendee's badge status (Attendee class has acess to Badge class because we have identify OneToOneRelationship in the Badge 'attendee' property)
        except (
            ObjectDoesNotExist
        ):  # raise the error if does not have badge yet
            Badge.objects.create(
                attendee=self
            )  # then proceed to create a badge and assign the 'attendee' property (specified in Badge) to the current instance


class Badge(models.Model):
    """
    The Badge model represents the badge an attendee gets to
    wear at the conference.

    Badge is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )
